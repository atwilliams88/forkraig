import React, {Component} from 'react';

class Account extends Component {
    state = {
        loading: true
    };

    async componentDidMount(){
        let res = await fetch("https://jsonplaceholder.typicode.com/posts")
        let data = await res.json();
        this.setState({
            loading: false,
            posts: data
        })
        let secondRes = await fetch(`https://jsonplaceholder.typicode.com/users/${this.props.id}`);
        let secondData = await secondRes.json();
        this.setState({
            ...this.state,
            user: secondData
        })
    }
    render() {
        const {loading, posts, user} = this.state;
        const { children } = this.props;
        return children({
            loading,
            posts,
            user,
        })

    }
}

export default Account;