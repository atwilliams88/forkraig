import React, { Component } from 'react';
import './App.css';
import Account from './components/utilities/account'
class App extends Component {
  render() {
    return (
        //id would come from match prop
      <Account id={1}>
        {({loading, posts, user})=> {
          if (loading) return "Loading ...";
          if (user && posts) return (
              <div>
                <h2>Some Person!</h2><br/>
                Name: {user.name}<br/>
                ID: {user.id}
                <br/>
                <hr/>
                <h2>Posts</h2>
                {posts.map((post)=>(
                    <div>
                      {post.title}
                    </div>
                ))}
              </div>
          );

          else return "hello world"
        }}
      </Account>
    );
  }
}

export default App;
